
this project is part of Declarations, an artistic research into the poetic materiality of the CSS web-standard.

this is an under development extension to collect and curate scrap of CSS around the web.

to use

1. first install node:

        sudo pacman -S nodejs npm

2. install web-ext

        sudo npm install --global web-ext

3. run your extension, inside of the folder of the extension

        web-ext run